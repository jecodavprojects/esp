class AddMoreFieldsToSettings < ActiveRecord::Migration[5.2]
  def up
    remove_column :settings, :advisor_message
    add_column :settings, :advisor_message, :string
    add_column :settings, :section_1, :string
    add_column :settings, :section_1_description, :text
    add_column :settings, :section_2, :string
    add_column :settings, :section_2_description, :text
    add_column :settings, :brief_introduction, :text
  end

  def down
    remove_column :settings, :advisor_message
    add_column :settings, :advisor_message, :text
    remove_column :settings, :section_1
    remove_column :settings, :section_1_description
    remove_column :settings, :section_2
    remove_column :settings, :section_2_description
    remove_column :settings, :brief_introduction
  end
end

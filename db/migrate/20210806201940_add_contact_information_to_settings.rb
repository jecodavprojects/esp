class AddContactInformationToSettings < ActiveRecord::Migration[5.2]
  def up
    add_column :settings, :address, :string
    add_column :settings, :email_address, :string
    add_column :settings, :contact_number_1, :string
    add_column :settings, :contact_number_2, :string
    add_column :posts, :is_pinned, :boolean, :default => false
  end

  def down
    remove_column :settings, :address
    remove_column :settings, :email_address
    remove_column :settings, :contact_number_1
    remove_column :settings, :contact_number_2
    remove_column :posts, :is_pinned
  end
end

class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :title
      t.text :description
      t.string :menu_1
      t.string :menu_2
      t.string :menu_3
      t.string :menu_4
      t.string :menu_5
      t.text :advisor_message

      t.timestamps
    end
  end
end

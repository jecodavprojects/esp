Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#index'
  resources :static_pages do
    collection do
      get   'about_us'
      get   'vision'
      get   'brief_report'
    end
  end
  match '/home',   to: 'static_pages#index',   via: 'get'
  match '/about_us',   to: 'static_pages#about_us',   via: 'get'
  match '/vision',   to: 'static_pages#vision',   via: 'get'
  match '/brief_report',   to: 'static_pages#brief_report',   via: 'get'
  match '/bullet_board',   to: 'static_pages#bullet_board',   via: 'get'
  match '/bulletin/:id',   to: 'static_pages#bulletin',   via: 'get',   as: 'post'
  match '/photo_gallery',   to: 'static_pages#photo_gallery',   via: 'get'
  match '/',   to: 'static_pages#create',   via: 'post'
end

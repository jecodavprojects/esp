class Post < ApplicationRecord
  # 1 = Bulletin
  # 2 = Provincial Education Advisors Message
  # 3 = Photo Gallery
  # 4 = Short Posts
  # 5 = About Us
  # 6 = Vision & Mission
  # 7 = Report
  has_many_attached :image_attachment
  has_many_attached :file_attachment

  def self.get_choices
   setting = Setting.first
   return [[1, setting.menu_2], [2, setting.advisor_message], [3, setting.section_1], [5, setting.menu_3], [6, setting.menu_4], [7, setting.menu_5]]
  end


  def self.bulletins
    Post.where(section_id: 1).order("created_at DESC")
  end

  def self.pinned_post
    Post.where(section_id: 1, is_pinned: true).order("created_at DESC")
  end

  def self.latest_post
    Post.where(section_id: 1, is_pinned: false).order("created_at DESC")
  end

  def self.advisor_message
    Post.where(section_id: 2).last
  end

  def self.photo_galleries
    Post.where(section_id: 3).order("created_at DESC")
  end

  def self.short_posts
    Post.where(section_id: 4).order("created_at DESC")
  end

  def self.about_us
    Post.where(section_id: 5).last
  end

  def self.vision_and_mission
    Post.where(section_id: 6).last
  end

  def self.brief_report
    Post.where(section_id: 7).last
  end
end

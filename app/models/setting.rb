class Setting < ApplicationRecord
  has_one_attached :logo_attachment
  has_one_attached :background_attachment
  has_one_attached :image_attachment
  DEFAULTS = {
    "title" => "East Sepik Province",
    "description" => "The province is located on the north of Papua New Guinea and the provincial capital is Wewak. The province has six electorates with political representatives to the National Parliament.",
    "menu_1" => "Home",
    "menu_2" => "Bulletin",
    "menu_3" => "About Us",
    "menu_4" => "Mission & Vision",
    "menu_5" => "Report",
    "advisor_message" => "Advisor Message",
    "section_1" => "Photo Gallery",
    "section_1_description" => "Photos of various events hosted by the East Sepik Province Division of Education.",
    "section_2" => "Get in Touch",
    "section_2_description" => "Tour function information without cross action media value quickly maximize timely deliverables.",
    "brief_introduction" => "The province is located on the north of Papua New Guinea and the provincial capital is Wewak. The province has six electorates with political representatives to the National Parliament.",
    "address" => "",
    "email_address" => "",
    "contact_number_1" => "",
    "contact_number_2" => ""
  }


  def self.get_settings_value(kval)
    # puts "working...."
    default_settings = Setting.first
    if default_settings
      if default_settings[kval]
          return default_settings[kval]
      end
    end

    return Setting::DEFAULTS[kval]
  end

end

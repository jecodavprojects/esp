class StaticPagesController < ApplicationController
  def index
    @advisor_message = Post.advisor_message
  end

  def about_us
    @post = Post.about_us
  end

  def vision
    @post = Post.vision_and_mission
  end

  def brief_report
    @post = Post.brief_report
  end

  def bullet_board
  end

  def photo_gallery
  end

  def bulletin
    @post = Post.find(params[:id])
    render 'show_post'
  end

  skip_before_action :verify_authenticity_token
  def create
    UserMailer.with(name: params[:name], email: params[:email], phone_number: params[:phone_number], subject: params[:subject], message: params[:message]).send_contact.deliver_now
    redirect_to root_path
  end
end

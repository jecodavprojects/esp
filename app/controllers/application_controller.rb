class ApplicationController < ActionController::Base
  before_action :get_settings


  def get_settings
    @setting = Setting.first
  end
end

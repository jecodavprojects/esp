class UserMailer < ApplicationMailer
  default from: "eastsepikeducation@gmail.com"

  def send_contact
    @name = params[:name]
    @email = params[:email]
    @phone_number = params[:phone_number]
    @message = params[:message]
    @subject = params[:subject]
    @settings = Setting.first
    if !@settings.logo_attachment.nil?
      if @settings.logo_attachment.attached?
        @email_template_filename = "esp" + @settings.logo_attachment.filename.extension_with_delimiter
        if ActiveStorage::Blob.service.respond_to?(:path_for)
          attachments.inline[@email_template_filename] = File.read(ActiveStorage::Blob.service.send(:path_for, @settings.logo_attachment.key))
        elsif ActiveStorage::Blob.service.respond_to?(:download)
          attachments.inline[@email_template_filename] = @settings.logo_attachment.download
        end
      end
    end
    mail(
      :from => "eastsepikeducation@gmail.com",
      :to => Setting.get_settings_value('email_address'),
      :subject => @subject
    )
  end

end
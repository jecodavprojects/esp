module ApplicationHelper
  def truncate_html_to_sentence(notes)
    if !notes.nil?
      truncate(ActionView::Base.full_sanitizer.sanitize(notes), length: 50, omission: '...')
    else
      ""
    end
  end

  def truncate_html_length_to_sentence(notes, char_length)
    if !notes.nil?
      truncate(ActionView::Base.full_sanitizer.sanitize(notes), length: char_length, omission: '...')
    else
      ""
    end
  end

end

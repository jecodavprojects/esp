ActiveAdmin.register Post do
  menu false
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :description, :user_id, :image_attachment
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :description, :user_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  member_action :delete_image_attachment, method: :delete do
    post = Post.find(params[:id])
    if !post.image_attachment.nil?
      if post.image_attachment.attached?
        ActiveStorage::Attachment.find(post.image_attachment.id).purge
      end
    end
    flash[:notice] = 'Successfully removed image attachment'
    redirect_back(fallback_location: params[:fallback_location])
  end

  index do
    selectable_column
    id_column
    column :title
    column "Description" do |p|
      truncate_html_length_to_sentence(raw(p.description), 500)
    end
    column "Created By" do |p|
      usr = AdminUser.find(p.user_id)
      link_to usr.email, admin_admin_user_path(p.user_id)
    end
    column :created_at
    actions
  end

  show do
    panel "Post Details" do
      attributes_table_for post do
        row "Attachment" do
          if !post.image_attachment.nil?
            if post.image_attachment.attached?
              span image_tag post.image_attachment
              span link_to 'Remove', delete_image_attachment_admin_post_url(:id => post.id, :fallback_location => admin_post_path(post.id)),
                           method: :delete,
                           data: { confirm: 'Are you sure?' }
            end
          end
        end
        row :title
        row "Created By" do |p|
          usr = AdminUser.find(p.user_id)
          link_to usr.email, admin_admin_user_path(p.user_id)
        end
        row :created_at
        row :updated_at
        row "Description" do |p|
          raw(p.description)
        end
      end
    end
  end

  filter :title
  filter :description
  filter :user_id, :as => :select, :collection => AdminUser.all.map{|u| [u.email, u.id]}
  filter :created_at

  form do |f|
    f.inputs do
      f.input :image_attachment, label: "Attachment", as: :file, :hint => (
        (
          li class: 'file input optional' do
            label 'Preview', class: 'label'
            span image_tag(f.object.image_attachment)
            span link_to 'Remove', delete_image_attachment_admin_post_url(:id => f.object.id, :fallback_location => edit_admin_post_path(f.object.id)),
                         method: :delete,
                         data: { confirm: 'Are you sure?' }
          end
        ) if f.object.image_attachment.attached? )
      f.input :title
      f.input :description, as: :html_editor
      f.input :section_id, label: "Section", :as => :select, :collection => Post.get_choices
      f.input :user_id, :input_html => { :value => current_admin_user.id }, as: :hidden
    end
    f.actions
  end

end

ActiveAdmin.register AdminUser do
  menu false
  permit_params :email, :password, :password_confirmation
  actions :all, :except => [:new, :edit, :destroy]

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column :actions do |item|
      links = []
      links << link_to('View', admin_admin_user_path(item.id))
      links.join(' ').html_safe
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
